import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";

function UpdateObat() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [obat, setObat] = useState("");
  const [expired, setExpired] = useState("");
  const [stock, setStock] = useState("");

  const updateObat = async () => {
    try {
      const data = {
        obat: obat,
        expired: expired,
        stock: stock,
      };
      console.log(data);
      axios.put(`/obat/${id}`, data);

      Swal.fire("Yes, You are Successful in editing the Teacher");
    } catch (err) {
      console.log(err);
    }
    navigate("/obat");
  };

  const save = () => {};

  const getById = async () => {
    const { data } = await axios.get(`/obat/id/${id}`);

    setObat(data.obat);
    setExpired(data.expired);
    setStock(data.stock);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div className="container mt-5">
      <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
        <div className="container">
          <form className="ml-4 mr-4" onSubmit={save}>
            <div className="mb-4 text-center">
              <h1>Edit Obat</h1>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Nama Obat</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Nama Obat"
                  className="form-control"
                  defaultdefaultValue={obat}
                  onChange={(e) => setObat(e.target.value)}
                />
              </div>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Expired</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  type="date"
                  placeholder="Expired"
                  className="form-control"
                  defaultdefaultValue={expired}
                  onChange={(e) => setExpired(e.target.value)}
                />
              </div>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Stock</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  type="number"
                  placeholder="Stock"
                  className="form-control"
                  defaultdefaultValue={stock}
                  onChange={(e) => setStock(e.target.value)}
                />
              </div>
            </div>

            <div className="d-flex justify-content-end align-items-center mt-2">
              <button onClick={updateObat} className="btn btn-success">
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdateObat;
