import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";

function UpdateDiagnosa() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [penyakit, setPenyakit] = useState("");

  const updateDiagnosa = async () => {
    try {
      const data = {
        penyakit: penyakit,
      };
      console.log(data);
      axios.put(`/penyakit/${id}`, data);

      Swal.fire("Yes, You are Successful in editing the Diagnosa");
    } catch (err) {
      console.log(err);
    }
    navigate("/diagnosa");
  };

  const save = () => {};

  const getById = async () => {
    const { data } = await axios.get(`/penyakit/id/${id}`);

    setPenyakit(data.penyakit);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div className="container mt-5">
      <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
        <div className="container">
          <form className="ml-4 mr-4" onSubmit={save}>
            <div className="mb-4 text-center">
              <h1>Edit Diagnosa</h1>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Nama Diagnosa</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Nama Diagnosa"
                  className="form-control"
                  defaultdefaultValue={penyakit}
                  onChange={(e) => setPenyakit(e.target.value)}
                />
              </div>
            </div>
            <div className="d-flex justify-content-end align-items-center mt-2">
              <button onClick={updateDiagnosa} className="btn btn-success">
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdateDiagnosa;
