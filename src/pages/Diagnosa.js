import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import Home from "../components/Home";
import { Link } from "react-router-dom";

function Diagnosa() {
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/penyakit/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  const navigate = useNavigate();
  const [penyakit, setPenyakit] = useState("");

  const addPenyakit = async () => {
    try {
      const formData = {
        penyakit: penyakit,
      };

      await axios.post(`/penyakit/`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/diagnosa");
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
    Swal.fire({
      icon: "success",
      title: "success add book",
      showConfirmButton: false,
      timer: 1500,
    });
  };
  const [diagnosa, setDiagnosa] = useState([]);

  const fetchGuru = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/api/penyakit/`,
        {}
      );
      if (status === 200) {
        setDiagnosa(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGuru();
  }, []);
  return (
    <div className="container">
      <Home />
      <div
        className="card"
        style={{ marginLeft: "15%", border: "none", color: "#35e97a" }}
      >
        <div className="card-header text-center mt-5  text-dark">
          <h4>Diagnosa Penyakit</h4>

          <button
            type="button"
            className="btn btn-success float-start"
            style={{ marginLeft: "13px" }}
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Tambah
          </button>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="exampleModalLabel">
                    Tambah Diagnosa Penyakit
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <form onSubmit={save}>
                    <div className="name mb-3">
                      <label className="form-label float-start">
                        <strong>Nama Penyakit</strong>
                      </label>
                      <div className="d-flex gap-3 input-group">
                        <input
                          className="form-control"
                          placeholder="Nama Penyakit"
                          defaultdefaultValue={penyakit}
                          onChange={(e) => setPenyakit(e.target.value)}
                        />
                      </div>
                    </div>
                  </form>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <div className="ms-2 me-2">||</div>
                  <button
                    type="submit"
                    onClick={addPenyakit}
                    className="btn btn-success"
                  >
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container table-responsive py-3">
            <table className="table table-bordered table-hover">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama Diagnosa</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {diagnosa.map((diagnosa, index) => (
                  <tr key={index} className="text-dark">
                    <th scope="row">{diagnosa.id}</th>
                    <td>{diagnosa.penyakit}</td>
                    <td>
                      <div
                        onClick={() => removePost(diagnosa.id)}
                        className="btn btn-danger   text-btn text-light "
                      >
                        <i className="fa-solid fa-trash"></i>
                      </div>
                      <Link
                        to={`/diagnosa/update/${diagnosa.id}`}
                        className="btn btn-info   text-btn text-light mx-3"
                      >
                        <i className="fa-solid fa-pen-to-square"></i>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Diagnosa;
