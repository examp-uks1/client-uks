import React, { useState } from "react";
import "../style/login.css";
import { Link } from "react-router-dom";
import { useLocation, useNavigate } from "react-router-dom";

function Register() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    username: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const register = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/log");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="hider">
      <h1 className="mt-4">Sistem Aplikasi UKS</h1>
      <img
        src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
        className="center mt-3"
      />
      <h2 className="text-center">Register</h2>
      <form className="login" method="post">
        <div className="txt_field">
          <label className="form-label">Email</label>
          <input
            type="text"
            id="username"
            className="form-control"
            onChange={handleOnChange}
            defaultdefaultValue={userRegister.username}
            placeholder="Enter email address"
          />
        </div>
        <div className="txt_field">
          <label className="form-label">Password</label>
          <input
            type="password"
            id="password"
            className="form-control"
            onChange={handleOnChange}
            defaultdefaultValue={userRegister.password}
            placeholder="Enter password"
          />
        </div>

        <button
          type="button"
          className="btn btn-success btn-color mb-2 w-100"
          onClick={register}
        >
          Register
        </button>
        <div className="signup_link">
          Have an Account? <Link to="/log">Signin</Link>
        </div>
      </form>
    </div>
  );
}

export default Register;
