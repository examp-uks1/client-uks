import React, { useEffect, useState } from "react";
import Home from "../components/Home";
import { instance as axios } from "../util/Api";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function Siswa() {
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/siswa/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  const navigate = useNavigate();
  const [nama, setNama] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [kelas, setKelas] = useState("");

  const addSiswa = async () => {
    try {
      const formData = {
        nama: nama,
        tanggalLahir: tanggalLahir,
        tempatLahir: tempatLahir,
        alamat: alamat,
        kelas: kelas,
      };

      await axios.post(`/siswa/`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Sukses Menambahkan Siswa",
      showConfirmButton: false,
      timer: 1500,
    });
    navigate("/siswa");
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
  };
  const [siswa, setSiswa] = useState([]);

  const fetchSiswa = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/api/murid/`,
        {}
      );
      if (status === 200) {
        setSiswa(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchSiswa();
  }, []);
  return (
    <div className="container">
      <div>
        <Home />
        <div
          className="card"
          style={{ marginLeft: "15%", border: "none", color: "#35e97a" }}
        >
          <div className="card-header text-center mt-5  text-dark">
            <h4>Daftar Siswa</h4>
            <button
              type="button"
              className="btn btn-success float-start"
              style={{ marginLeft: "13px" }}
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              Tambah Siswa
            </button>
            <div
              className="modal fade"
              id="exampleModal"
              tabIndex="-1"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h1 className="modal-title fs-5" id="exampleModalLabel">
                      Tambah Siswa
                    </h1>
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={save}>
                      <div className="name mb-3">
                        <label className="form-label">
                          <strong>Nama Siswa</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            className="form-control"
                            placeholder="Nama Siswa"
                            defaultdefaultValue={nama}
                            onChange={(e) => setNama(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label">
                          <strong>Tempat Lahir</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            placeholder="Tempat Lahir"
                            className="form-control"
                            defaultdefaultValue={tempatLahir}
                            onChange={(e) => setTempatLahir(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label">
                          <strong>Tanggal Lahir</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            type="date"
                            className="form-control"
                            defaultdefaultValue={tanggalLahir}
                            onChange={(e) => setTanggalLahir(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label">
                          <strong>Alamat</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            placeholder="Alamat"
                            className="form-control"
                            defaultdefaultValue={alamat}
                            onChange={(e) => setAlamat(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label">
                          <strong>Kelas</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            type="number"
                            placeholder="Kelas"
                            className="form-control"
                            defaultdefaultValue={kelas}
                            onChange={(e) => setKelas(e.target.value)}
                          />
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-bs-dismiss="modal"
                    >
                      Batal
                    </button>
                    <div className="ms-2 me-2">||</div>
                    <button
                      type="submit"
                      onClick={addSiswa}
                      className="btn btn-success"
                    >
                      Simpan
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="container table-responsive py-5">
              <table className="table table-bordered table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Siswa</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">Tempat Lahir</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {siswa.map((siswa, index) => (
                    <tr key={index} className="text-dark">
                      <th scope="row">{siswa.id}</th>
                      <td>{siswa.nama}</td>
                      <td>{siswa.kelas}</td>
                      <td>{siswa.tanggalLahir}</td>
                      <td>{siswa.tempatLahir}</td>
                      <td>{siswa.alamat}</td>
                      <td>
                        <div
                          onClick={() => removePost(siswa.id)}
                          className="btn btn-danger   text-btn text-light "
                        >
                          <i className="fa-solid fa-trash"></i>
                        </div>
                        <Link
                          to={`/siswa/update/${siswa.id}`}
                          className="btn btn-info   text-btn text-light mx-3"
                        >
                          <i className="fa-solid fa-pen-to-square"></i>
                        </Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Siswa;
