import React from "react";
import Home from "../components/Home";
import AddPasien from "../components/AddPasien";
import FilterTanggal from "../components/FilterTanggal";

function Periksa() {
  return (
    <div className="container">
      <Home />
      <div
        className="card-daftar"
        style={{ marginLeft: "15%", marginTop: "50px" }}
      >
        <div className="card">
          <div className="card-header bg-success d-flex justify-content-between">
            <h4 className="text-light">Filter Rekap Data</h4>
            <FilterTanggal />
          </div>
          <div className="card-body">
            <div className="card-title">
              <h1 className="text-center container pt-2">
                <i className="fa-solid fa-circle-exclamation fa-2xl mt-5"></i>
                <p className="pt-3 mt-4">
                  Filter terlebih dahulu sesuai tanggal yang diinginkan.
                </p>
              </h1>
            </div>
          </div>
        </div>
        <div className="card " style={{ marginTop: "30px" }}>
          <div className="card-header bg-success  justify-content-between">
            <h4 className="text-light">Filter Rekap Data</h4>
            <AddPasien />
          </div>
          <div className="card-body">
            <div className="card-title">
              <div className="card-body">
                <div className="card-title">
                  <div className="table-responsive ">
                    <table className="table table-bordered table-hover">
                      <thead className="thead-dark">
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Nama Pasien</th>
                          <th scope="col">Status Pasien</th>
                          <th scope="col">Jabatan</th>
                          <th scope="col">Tanggal</th>
                          <th scope="col">Keterangan</th>
                          <th scope="col">Status</th>
                          <th scope="col">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                          <td>@mdo</td>
                          <td>@mdo</td>
                          <td>@mdo</td>
                          <td>@mdo</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Periksa;
