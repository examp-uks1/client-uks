import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";

function UpdateTindakan() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [tindakan, setTindakan] = useState("");

  const updateTindakan = async () => {
    try {
      const data = {
        tindakan: tindakan,
      };
      console.log(data);
      axios.put(`/tindakan/${id}`, data);

      Swal.fire("Yes, You are Successful in editing the Tindakan");
    } catch (err) {
      console.log(err);
    }
    navigate("/tindakan");
  };

  const save = () => {};

  const getById = async () => {
    const { data } = await axios.get(`/tindakan/id/${id}`);

    setTindakan(data.tindakan);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div className="container mt-5">
      <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
        <div className="container">
          <form className="ml-4 mr-4" onSubmit={save}>
            <div className="mb-4 text-center">
              <h1>Edit Tindakan </h1>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Nama Tindakan</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Tindakan"
                  className="form-control"
                  defaultdefaultValue={tindakan}
                  onChange={(e) => setTindakan(e.target.value)}
                />
              </div>
            </div>
            <div className="d-flex justify-content-end align-items-center mt-2">
              <button onClick={updateTindakan} className="btn btn-success">
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdateTindakan;
