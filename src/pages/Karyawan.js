import React, { useEffect, useState } from "react";
import Home from "../components/Home";
import { instance as axios } from "../util/Api";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function Karyawan() {
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/karyawan/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  const navigate = useNavigate();
  const [nama, setNama] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [alamat, setAlamat] = useState("");

  const addKaryawan = async () => {
    try {
      const formData = {
        nama: nama,
        tanggalLahir: tanggalLahir,
        tempatLahir: tempatLahir,
        alamat: alamat,
      };

      await axios.post(`/karyawan/`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Sukses Menambahkan Karyawan",
      showConfirmButton: false,
      timer: 1500,
    });
    navigate("/karyawan");
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
    Swal.fire({
      icon: "success",
      title: "success add book",
      showConfirmButton: false,
      timer: 1500,
    });
  };
  const [karyawan, setKaryawan] = useState([]);

  const fetchKaryawan = async () => {
    try {
      const { data, status } = await axios.get(`/karyawan/`, {});
      if (status === 200) {
        setKaryawan(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchKaryawan();
  }, []);
  return (
    <div className="container">
      <div>
        <Home />
        <div
          className="card"
          style={{ marginLeft: "15%", border: "none", color: "#35e97a" }}
        >
          <div className="card-header text-center mt-5 float-start  text-dark">
            <h4>Daftar Karyawan</h4>

            <button
              type="button"
              className="btn btn-success float-start"
              style={{ marginLeft: "13px" }}
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              Tambah Karyawan
            </button>

            <div
              className="modal fade"
              id="exampleModal"
              tabIndex="-1"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h1 className="modal-title fs-5" id="exampleModalLabel">
                      Tambah Karyawan
                    </h1>
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={save}>
                      <div className="name mb-3">
                        <label className="form-label float-start">
                          <strong>Nama Karyawan</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            className="form-control"
                            placeholder="Nama Karyawan"
                            defaultdefaultValue={nama}
                            onChange={(e) => setNama(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label float-start">
                          <strong>Tempat Lahir</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            placeholder="Tempat Lahir"
                            className="form-control"
                            defaultdefaultValue={tempatLahir}
                            onChange={(e) => setTempatLahir(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label float-start">
                          <strong>Tanggal Lahir</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            type="date"
                            className="form-control"
                            defaultdefaultValue={tanggalLahir}
                            onChange={(e) => setTanggalLahir(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="name mb-3">
                        <label className="form-label float-start">
                          <strong>Alamat</strong>
                        </label>
                        <div className="d-flex gap-3 input-group">
                          <input
                            placeholder="Alamat"
                            className="form-control"
                            defaultdefaultValue={alamat}
                            onChange={(e) => setAlamat(e.target.value)}
                          />
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-bs-dismiss="modal"
                    >
                      Batal
                    </button>
                    <div className="ms-2 me-2">||</div>
                    <button
                      type="submit"
                      onClick={addKaryawan}
                      className="btn btn-success"
                    >
                      Simpan
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="container table-responsive py-5">
              <table className="table table-bordered table-hover ">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">Tempat Lahir</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {karyawan.map((karyawan, index) => (
                    <tr key={index} className="text-dark">
                      <th scope="row">{karyawan.id}</th>
                      <td>{karyawan.nama}</td>
                      <td>{karyawan.tanggalLahir}</td>
                      <td>{karyawan.tempatLahir}</td>
                      <td>{karyawan.alamat}</td>
                      <td>
                        <div
                          onClick={() => removePost(karyawan.id)}
                          className="btn btn-danger   text-btn text-light "
                        >
                          <i className="fa-solid fa-trash"></i>
                        </div>
                        <Link
                          to={`/karyawan/update/${karyawan.id}`}
                          className="btn btn-info   text-btn text-light mx-3"
                        >
                          <i className="fa-solid fa-pen-to-square"></i>
                        </Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Karyawan;
