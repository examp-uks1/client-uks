import React from "react";
import { Link } from "react-router-dom";
import "../style/welcome.css";

function Welcome() {
  return (
    <div className="welcome">
      <div className="container text-center">
        <div
          className="text-center"
          style={{ textAlign: "center", marginTop: "50px" }}
        >
          <h1 className="text-center">
            Welcome to Ave BookStore
            <h3>Fisik Hanyalah Fatamorgana Yang Akan Lenyap Ditelan Usia.</h3>
          </h1>
          <img
            src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
            className="center mt-5"
          />
        </div>
      </div>
      <div className="col-6">
        {/* <div className="p-3">
              <img
                src="https://img.freepik.com/free-vector/colorful-spring-background_23-2148837212.jpg?w=740&t=st=1684308743~exp=1684309343~hmac=39a73135921ed37080e70851dead4974a42ad257e3844381b7bf6ed7a99e7e6c"
                className="img-fluid"
                alt=""
              />
            </div> */}
      </div>
      <div className="col-6">
        <div className="p-3"></div>
      </div>
      <div className="text-center" style={{ marginTop: "50px" }}>
        <Link to="/dashboard">
          <button type="button" className="btn btn-success btn-lg">
            Lanjut
          </button>
        </Link>
      </div>
    </div>
  );
}

export default Welcome;
