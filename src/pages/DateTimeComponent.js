import React, { useEffect, useState } from "react";

function DateTime() {
  const [clockState, setClockState] = useState();
  const now = new Date();
  const formattedDate = now.toLocaleDateString();
  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
    }, 1000);
  }, []);

  return (
    <div className="text-center text-light mt-5">
      <h6>{formattedDate}</h6>
      <h5>{clockState}</h5>
    </div>
  );
}

export default DateTime;
