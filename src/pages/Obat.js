import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/Api";
import Home from "../components/Home";
import { Link } from "react-router-dom";

function Tindakan() {
  const navigate = useNavigate();
  const [obat, setObat] = useState("");
  const [expired, setExpired] = useState("");
  const [stock, setStock] = useState("");
  const [obatan, setObatan] = useState([]);

  const addObat = async () => {
    try {
      const formData = {
        obat: obat,
        expired: expired,
        stock: stock,
      };

      await axios.post(`/obat/`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/obat");
  };

  const submit = (event) => {
    event.preventDefault();
    addObat();
    Swal.fire({
      icon: "success",
      title: " Sukses tambah Tindakan",
      showConfirmButton: false,
      timer: 800,
    });
  };

  const fetchObat = async () => {
    try {
      const { data, status } = await axios.get(`/obat/`, {});
      if (status === 200) {
        setObatan(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/obat/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  useEffect(() => {
    fetchObat();
  }, []);

  return (
    <div className="container">
      <Home />
      <div
        className="card"
        style={{ marginLeft: "15%", border: "none", color: "#35e97a" }}
      >
        <div className="card-header text-center mt-5  text-dark">
          <h4>Daftar Obat</h4>

          <button
            type="button"
            className="btn btn-success float-start"
            style={{ marginLeft: "13px" }}
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Tambah
          </button>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="exampleModalLabel">
                    Tambah Obat
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="name mb-3">
                      <label className="form-label float-start">
                        <strong>Nama Obat</strong>
                      </label>
                      <div className="d-flex gap-3 input-group">
                        <input
                          className="form-control"
                          placeholder="Nama Obat"
                          defaultdefaultValue={obat}
                          onChange={(e) => setObat(e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="name mb-3">
                      <label className="form-label float-start">
                        <strong>Stock</strong>
                      </label>
                      <div className="d-flex gap-3 input-group">
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Stock"
                          defaultdefaultValue={stock}
                          onChange={(e) => setStock(e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="name mb-3">
                      <label className="form-label float-start">
                        <strong>Tanggal Expired</strong>
                      </label>
                      <div className="d-flex gap-3 input-group">
                        <input
                          type="date"
                          className="form-control"
                          defaultdefaultValue={expired}
                          onChange={(e) => setExpired(e.target.value)}
                        />
                      </div>
                    </div>
                  </form>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <div className="ms-2 me-2">||</div>
                  <button onClick={submit} className="btn btn-success">
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="container table-responsive py-3">
            <table className="table table-bordered table-hover">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama Obat</th>
                  <th scope="col">Stock</th>
                  <th scope="col">Expired</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {obatan.map((data, index) => (
                  <tr key={index} className="text-dark">
                    <th scope="row">{data.id}</th>
                    <td>{data.obat}</td>
                    <td>{data.stock}</td>
                    <td>{data.expired}</td>
                    <td>
                      <div
                        onClick={() => removePost(data.id)}
                        className="btn btn-danger   text-btn text-light "
                      >
                        <i className="fa-solid fa-trash"></i>
                      </div>
                      <Link
                        to={`/obat/update/${data.id}`}
                        className="btn btn-info   text-btn text-light mx-3"
                      >
                        <i className="fa-solid fa-pen-to-square"></i>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Tindakan;
