import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";

function UpdateGuru() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [nama, setNama] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [alamat, setAlamat] = useState("");

  const updateGuru = async () => {
    try {
      const data = {
        nama: nama,
        tanggalLahir: tanggalLahir,
        tempatLahir: tempatLahir,
        alamat: alamat,
      };
      console.log(data);
      axios.put(`/guru/${id}`, data);

      Swal.fire("Yes, You are Successful in editing the Teacher");
    } catch (err) {
      console.log(err);
    }
    navigate("/guru");
  };

  const save = () => {};

  const getById = async () => {
    const { data } = await axios.get(`/guru/id/${id}`);

    setNama(data.nama);
    setTanggalLahir(data.tanggalLahir);
    setTempatLahir(data.tempatLahir);
    setAlamat(data.alamat);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div className="container mt-5">
      <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
        <div className="container">
          <form className="ml-4 mr-4" onSubmit={save}>
            <div className="mb-4 text-center">
              <h1>Edit Guru</h1>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Nama Guru</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Nama Guru"
                  className="form-control"
                  defaultdefaultValue={nama}
                  onChange={(e) => setNama(e.target.value)}
                />
              </div>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Tempat Lahir</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Tempat Lahir"
                  className="form-control"
                  defaultdefaultValue={tempatLahir}
                  onChange={(e) => setTempatLahir(e.target.value)}
                />
              </div>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Tempat Lahir</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Tanggal Lahir"
                  className="form-control"
                  defaultdefaultValue={tanggalLahir}
                  onChange={(e) => setTempatLahir(e.target.value)}
                />
              </div>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Alamat</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Alamat"
                  className="form-control"
                  defaultdefaultValue={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />
              </div>
            </div>
            <div className="d-flex justify-content-end align-items-center mt-2">
              <button onClick={updateGuru} className="btn btn-success">
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdateGuru;
