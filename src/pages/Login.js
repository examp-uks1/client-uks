import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

function Login() {
  const location = useLocation();
  const navigate = useNavigate();
  const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  const [userLogin, setUserLogin] = useState({
    username: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signIn = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        // console.log(data.token);
        // console.log(data.userData.id);
        // console.log(data.userData.role);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          Toast.fire({
            icon: "success",
            title: "Signed in successfully",
          });
          navigate("/welc ");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="hider">
      <h1 className="mt-4">Sistem Aplikasi UKS</h1>
      <img
        src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
        className="center mt-3"
      />
      <h2 className="text-center">Login</h2>
      <form className="login" method="post">
        <div className="txt_field">
          <label className="form-label">Email</label>
          <input
            type="text"
            id="username"
            onChange={handleOnChange}
            defaultdefaultValue={userLogin.username}
            placeholder="Enter email address"
          />
        </div>
        <div className="txt_field">
          <label className="form-label">Password</label>
          <input
            type="password"
            id="password"
            onChange={handleOnChange}
            defaultdefaultValue={userLogin.password}
            placeholder="Enter password"
          />
        </div>
        <button
          type="button"
          className="btn btn-success btn-color mb-2 w-100"
          onClick={signIn}
        >
          Login
        </button>
      </form>
    </div>
  );
}

export default Login;
