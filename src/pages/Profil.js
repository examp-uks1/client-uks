import React from "react";
import "../style/profil.css";

function Profil() {
  return (
    <div className="profilll">
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      />

      <div class="cards">
        <img
          src="https://www.w3schools.com/howto/img_avatar.png"
          //   style={{ marginLeft: "1px" }}
          alt="image"
        />
        <h1>Aveceena</h1>

        <button
          type="button"
          className="btn btn-success "
          style={{ marginLeft: "13px" }}
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
        >
          Edit Profil
        </button>

        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="exampleModalLabel">
                  Edit Profil
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="name mb-3">
                    <label className="form-label float-start">
                      <strong>Edit Password</strong>
                    </label>
                    <div className="d-flex gap-3 input-group">
                      <input
                        className="form-control"
                        placeholder="Edit Pasword"
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <input
                      type="file"
                      className="form-control"
                      placeholder="Edit Profil"
                    />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-bs-dismiss="modal"
                >
                  Batal
                </button>
                <div className="ms-2 me-2">||</div>
                <button type="submit" className="btn btn-success">
                  Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profil;
