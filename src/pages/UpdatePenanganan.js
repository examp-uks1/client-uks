import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";

function UpdatePenanganan() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [pertolongan, setPertolongan] = useState("");

  const updatePertolongan = async () => {
    try {
      const data = {
        pertolongan: pertolongan,
      };
      console.log(data);
      axios.put(`/penanganan/${id}`, data);

      Swal.fire("Yes, You are Successful in editing the Penanganan");
    } catch (err) {
      console.log(err);
    }
    navigate("/penanganan");
  };

  const save = () => {};

  const getById = async () => {
    const { data } = await axios.get(`/penanganan/id/${id}`);

    setPertolongan(data.penyakit);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div className="container mt-5">
      <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
        <div className="container">
          <form className="ml-4 mr-4" onSubmit={save}>
            <div className="mb-4 text-center">
              <h1>Edit Penanganan Pertama</h1>
            </div>
            <div className="name mb-3">
              <label className="form-label">
                <strong>Nama Penanganan Pertama</strong>
              </label>
              <div className="d-flex gap-3 input-group">
                <input
                  placeholder="Nama Penanganan Pertama"
                  className="form-control"
                  defaultdefaultValue={pertolongan}
                  onChange={(e) => setPertolongan(e.target.value)}
                />
              </div>
            </div>
            <div className="d-flex justify-content-end align-items-center mt-2">
              <button onClick={updatePertolongan} className="btn btn-success">
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdatePenanganan;
