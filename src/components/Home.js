import React from "react";
import "../style/dashboard.css";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import DateTimeComponent from "../pages/DateTimeComponent";

function Home() {
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <div>
      <p className="profile mt-1" style={{ marginLeft: "106%" }}>
        <Link to={"/profil"}>
          <img
            style={{ borderRadius: "50px" }}
            className="w3-circle"
            src="https://www.w3schools.com/howto/img_avatar.png"
            width={"50px"}
            alt="Car"
          />
        </Link>
      </p>
      <div className="sidebar ">
        <h5 className="text-light text-center mt-3">
          SISTEM APLIKASI UKS <br /> SMPN 1 SEMARANG
        </h5>
        <hr />
        <Link to={"/dashboard"}>
          <i className="fa fa-fw fa-home "></i> Dashboard
        </Link>
        <Link to={"/pasien"}>
          <i className="fa-solid fa-stethoscope"></i> Periksa Pasien
        </Link>
        <div className="dropdown">
          <a href="//" data-bs-toggle="dropdown">
            <i className="fa-solid fa-arrow-down"></i>
            Data
          </a>
          <ul className="dropdown-menu bg-dark">
            <li>
              <Link to={"/guru"}>
                <i className="fa-solid fa-graduation-cap"></i> Data Guru
              </Link>
            </li>
            <li>
              <Link to={"/siswa"}>
                <i
                  className="fa-solid fa-user"
                  style={{ marginLeft: "5px" }}
                ></i>{" "}
                Data Siswa
              </Link>
            </li>
            <li>
              <Link to={"/karyawan"}>
                <i
                  className="fa-solid fa-users"
                  style={{ marginLeft: "5px" }}
                ></i>{" "}
                Data Karyawan
              </Link>
            </li>
          </ul>
        </div>

        <Link to={"/diagnosa"}>
          <i className="fa-solid fa-person-dots-from-line"></i> Diagnosa
        </Link>
        <Link to={"/penanganan"}>
          <i className="fa-solid fa-hands-holding"></i> Penanganan Pertama
        </Link>
        <Link to={"/tindakan"}>
          <i className="fa-solid fa-location-crosshairs"></i> Tindakan
        </Link>
        <Link to={"/obat"}>
          <i className="fa-solid fa-capsules"></i> Daftar Obat P3K
        </Link>
        <DateTimeComponent />
        <Link to={"/log"} onClick={logout}>
          <i
            className="fa-solid fa-right-from-bracket"
            style={{ marginTop: "110px" }}
            on
          ></i>{" "}
          Log out
        </Link>
      </div>

      {/* <div className="main">
        <h2>Sidebar with Icons</h2>
        <p>This side navigation is of full height (100%) and always shown.</p>
        <p>
          Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset
          concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur
          ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec
          et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.
        </p>
        <p>
          Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset
          concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur
          ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec
          et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.
        </p>
      </div> */}
    </div>
  );
}

export default Home;
