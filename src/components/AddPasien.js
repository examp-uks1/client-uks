import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/Api";
import Home from "./Home";
import { useNavigate } from "react-router-dom";

function AddPasien() {
  const [data, setData] = useState([]);
  const [statusPasien, setStatusPasien] = useState([]);
  const [namaPasien, setNamaPasien] = useState(0);
  const [keluhan, setKeluhan] = useState("");
  const navigate = useNavigate();

  const addPasien = async () => {
    try {
      const formData = {
        status: { id: statusPasien },
        nama: { id: namaPasien },
        keluhan: keluhan,
      };

      await axios.post(`/status/`, formData, {});
    } catch (err) {
      console.log(err);
    }
    navigate("/pasien");
  };

  const fetchStatus = async () => {
    try {
      const { data, status } = await axios.get(`/status/`);
      if (status === 200) {
        setStatusPasien(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const fetchSiswa = async () => {
    try {
      const { data, status } = await axios.get(`/murid/`);
      if (status === 200) {
        setData(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const fetchGuru = async () => {
    try {
      const { data, status } = await axios.get(`/teacher/`);
      if (status === 200) {
        setData(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const fetchKaryawan = async () => {
    try {
      const { data, status } = await axios.get(`/karyawan/`);
      if (status === 200) {
        setData(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const changeStatusPasien = (e) => {
    setStatusPasien(e.target.value);
    if (e.target.value === "Siswa") {
      fetchSiswa();
    } else if (e.target.value === "Karyawan") {
      fetchKaryawan();
    } else if (e.target.value === "Guru") {
      fetchGuru();
    }
  };

  const save = (e) => {
    e.preventDefault();
    AddPasien();
  };

  useEffect(() => {
    fetchStatus();
    fetchSiswa();
    fetchGuru();
    fetchKaryawan();
  }, []);

  return (
    <div className="container">
      {/* Button trigger modal */}
      <button
        type="button"
        className="btn btn-primary float-end"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal2"
        id="AddPasien"
        style={{ marginLeft: "200px" }}
      >
        <i className="fa-solid fa-plus"></i> Tambah
      </button>

      {/* Modal */}
      <div
        className="modal fade"
        id="exampleModal2"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="exampleModalLabel">
                <b>Tambah Daftar Pasien</b>
              </h1>
            </div>
            <div className="modal-body">
              <form onSubmit={save}>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Status Pasien</b>
                  </label>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                    onClick={changeStatusPasien}
                  >
                    <option value="Pilih Status" disabled>
                      {" "}
                      Pilih Status
                    </option>
                    <option value="Guru">Guru</option>
                    <option value="Siswa">Siswa</option>
                    <option value="Karyawan">Karyawan</option>
                  </select>
                </div>
                {/* nama pasien */}
                <div className="mb-3">
                  <label className="form-label">
                    <b>Nama Pasien</b>
                  </label>
                  <select class="form-select select2">
                    <option
                      value="selected"
                      onChange={(e) => setNamaPasien(e.target.value)}
                      required
                    >
                      Pilih Pasien
                    </option>
                    {data.map((data, i) => (
                      <option value={data.id} key={i}>
                        {" "}
                        {data.nama}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="mb-3">
                  <label className="form-label">
                    <b>Keluhan Pasien</b>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Keluhan Pasien"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddPasien;
