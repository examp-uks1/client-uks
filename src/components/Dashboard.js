import React, { useEffect, useState } from "react";
import Home from "../components/Home";
import { instance as axios } from "../util/Api";

function Dashboard() {
  const [siswa, setSiswa] = useState([]);

  const fetchSiswa = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/api/murid/`,
        {}
      );
      if (status === 200) {
        setSiswa(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchSiswa();
  }, []);

  return (
    <div className="container">
      <div>
        <Home />{" "}
      </div>
      <div className="main row">
        <div
          className="card shadow m-4 col"
          style={{ width: "340px", height: "100px" }}
        >
          <div className="card-body">
            <h5 className="card-title fs-6 text-center">Daftar Pasien Guru</h5>
            <h6
              className="card-subtitle text-center fs-2 mb-2"
              style={{ color: "#35e97a" }}
            >
              <span className="fa-stack fa-xs">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Guru
            </h6>
          </div>
        </div>
        <div
          className="card shadow m-4 col"
          style={{ width: "340px", height: "100px" }}
        >
          <div className="card-body">
            <h5 className="card-title fs-6 text-center">Daftar Pasien Guru</h5>
            <h6
              className="card-subtitle text-center fs-2 mb-2"
              style={{ color: "#35e97a" }}
            >
              <span className="fa-stack fa-xs">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Guru
            </h6>
          </div>
        </div>
        <div
          className="card shadow m-4 col"
          style={{ width: "340px", height: "100px" }}
        >
          <div className="card-body">
            <h5 className="card-title fs-6 text-center">Daftar Pasien Guru</h5>
            <h6
              className="card-subtitle text-center fs-2 mb-2"
              style={{ color: "#35e97a" }}
            >
              <span className="fa-stack fa-xs">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Guru
            </h6>
          </div>
        </div>
        <div
          className="card"
          style={{ marginRight: "4%", border: "none", color: "#35e97a" }}
        >
          <div className="card-header text-center mt-5  text-dark">
            <h4>Riwayat Pasien</h4>
            <div className="container table-responsive py-5">
              <table className="table table-bordered table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pasien</th>
                    <th scope="col">Status Pasien</th>
                    <th scope="col">Tanggal</th>
                  </tr>
                </thead>
                <tbody>
                  {siswa.map((siswa, index) => (
                    <tr key={index} className="text-dark">
                      <th scope="row">{siswa.id}</th>
                      <td>{siswa.nama}</td>
                      <td>{siswa.alamat}</td>
                      <td>{siswa.kelas}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
