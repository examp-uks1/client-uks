import React from "react";

function FilterTanggal() {
  return (
    <div>
      <button
        type="button"
        className="btn btn-success float-start"
        style={{ marginLeft: "13px" }}
        data-bs-toggle="modal"
        data-bs-target="#exampleModal1"
      >
        Filter Tanggal
      </button>

      <div
        className="modal fade"
        id="exampleModal1"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="exampleModalLabel">
                Filter Rekap Data Pasien
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form />
              <div className="name mb-3">
                <label className="form-label">
                  <strong>Dari Tanggal:</strong>
                </label>
                <div className="d-flex gap-3 input-group">
                  <input type="date" className="form-control" defaultValue="" />
                </div>
              </div>
              <div className="name mb-3">
                <label className="form-label">
                  <strong>Sampai Tanggal:</strong>
                </label>
                <div className="d-flex gap-3 input-group">
                  <input type="date" className="form-control" defaultValue="" />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <div className="ms-2 me-2">||</div>
              <button type="submit" className="btn btn-success">
                Simpan
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FilterTanggal;
