import { Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import Login from "./pages/Login";
import Dashboard from "./components/Dashboard";
import Register from "./pages/Register";
import Periksa from "./pages/Periksa";
import Guru from "./pages/Guru";
import Siswa from "./pages/Siswa";
import Karyawan from "./pages/Karyawan";
import UpdateGuru from "./pages/UpdateGuru";
import UpdateSiswa from "./pages/UpdateSiswa";
import UpdateKaryawan from "./pages/UpdateKaryawan";
import Diagnosa from "./pages/Diagnosa";
import Penanganan from "./pages/Penanganan";
import UpdateDiagnosa from "./pages/UpdateDiagnosa";
import UpdatePenanganan from "./pages/UpdatePenanganan";
import Tindakan from "./pages/Tindakan";
import UpdateTindakan from "./pages/UpdateTindakan";
import Obat from "./pages/Obat";
import UpdateObat from "./pages/UpdateObat";
import Welcome from "./pages/Welcome";
import Profil from "./pages/Profil";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/log" element={<Login />} />
        <Route path="" element={<Register />} />

        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/pasien" element={<Periksa />} />
        <Route path="/welc" element={<Welcome />} />
        <Route path="/guru" element={<Guru />} />
        <Route path="/siswa" element={<Siswa />} />
        <Route path="/karyawan" element={<Karyawan />} />
        <Route path="/diagnosa" element={<Diagnosa />} />
        <Route path="/guru/update/:id" element={<UpdateGuru />} />
        <Route path="/siswa/update/:id" element={<UpdateSiswa />} />
        <Route path="/karyawan/update/:id" element={<UpdateKaryawan />} />
        <Route path="/diagnosa/update/:id" element={<UpdateDiagnosa />} />
        <Route path="/penanganan/update/:id" element={<UpdatePenanganan />} />
        <Route path="/tindakan/update/:id" element={<UpdateTindakan />} />
        <Route path="/obat/update/:id" element={<UpdateObat />} />
        <Route path="/penanganan" element={<Penanganan />} />
        <Route path="/tindakan" element={<Tindakan />} />
        <Route path="/obat" element={<Obat />} />
        <Route path="/profil" element={<Profil />} />
      </Routes>
    </div>
  );
}

export default App;
